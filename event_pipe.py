import datetime
import json
import os
import os.path as osp
import pathlib

import cv2
import numpy as np
from KukeFace.module.FaceAlignment import AlignmentTool
from KukeFace.module.FaceDetection import FaceDetector
from KukeFace.module.FaceDetection.FaceDetectionConfig import MODEL_PATH
from KukeFace.module.FaceQuality import FQAssessor
from KukeFace.utils.download import MODELS

from specification import get_attribute_table, get_specification_table

class event_pipe(object):
    def __init__(self, config):
        super().__init__()
        self.config = config
        fd_config = {
            'stage1':{
                'input_res': 64,
                'model_path': osp.expanduser(MODEL_PATH['IRFDetector_stage2']),
                'threshold': 0.4,
                'K': 1},
            'stage2': {}
        }
        self.face_detector = FaceDetector('IR')
        self.face_detector.resetConfig(fd_config)
        self.aligntool = AlignmentTool()
        self.fq_assessor = FQAssessor('IR')
        self.timestamp = str(datetime.datetime.now())

    def run(self):
        spec = get_specification_table()
        spec = self.update_model_version(spec)
        ir_paths, depth_paths = self.setup_img_paths(self.config['event_config']['root'])
        out_dir = osp.join(self.config['raw_config']['out_dir'])
        self.normalized_dir = osp.join(out_dir, 'normalized')
        os.makedirs(self.normalized_dir, exist_ok=True)
        attributes = self.parse_attribute(ir_paths, depth_paths)
        self.dump_attributes(spec, attributes)

    def update_model_version(self, spec):
        spec['model']['FQ']['headpose'] = MODELS['IR_HeadPose']
        spec['model']['FQ']['blurriness'] = MODELS['IR_Blurriness']
        spec['model']['FQ']['occlusion'] = MODELS['IR_Occlusion']
        spec['model']['FD']['stage1'] = MODELS['IRFDetector_stage1']
        spec['model']['FD']['stage2'] = MODELS['IRFDetector_stage2']
        spec['model']['FR'] = MODELS['MK1_IRFR_EFFICIENTB0']
        return spec


    def setup_img_paths(self, root_dir):
        ir_dir = osp.join(root_dir, 'events', 'ir')
        ir_paths = [str(p) for p in pathlib.Path(ir_dir).rglob('*.*[gG]')]
        depth_dir = osp.join(root_dir, 'events', 'depth')
        depth_paths = [str(p) for p in pathlib.Path(depth_dir).rglob('*.*[gG]')]
        return ir_paths, depth_paths


    def parse_attribute(self, ir_paths, depth_paths):
        """
        1. rotate
        2. get landmark from irz image
        3. get distance from irz, depth image
        3. normalize
        """
        attributes = []
        root_dir = self.config['event_config']['root']
        normalized_dir = self.config['event_config']['out_normalized_dir']
        for ir_path, depth_path in zip(ir_paths, depth_paths):
            ir_img = cv2.imread(ir_path)
            depth_img = cv2.imread(depth_path, -1)
            irz_img = self.get_irz_img(ir_img, depth_img)
            boxes, landmarks = self.face_detector.getBoxesAndLandmarks(irz_img)
            if len(boxes) == 0:
                self.log.write(f'{ir_path}: detect no face.')
                continue
            else:
                box, landmark = boxes[0], landmarks[0]
                ir_aligned = self.aligntool.getAlignedImage(ir_img, landmark, (112, 112))
                ir_normalized_face = self.normailze_ir(ir_aligned)
                normalized_face_filename = osp.basename(ir_path)[:-4] + '.jpg'
                if normalized_dir:
                    target_dir = osp.dirname(ir_path).replace(root_dir, normalized_dir)
                    os.makedirs(target_dir, exist_ok=True)
                    cv2.imwrite(osp.join(target_dir, normalized_face_filename), ir_normalized_face)
                
                table = get_attribute_table()
                table['filename'] = ir_path
                table['attribute']['id'] = ""

                # get depth
                table['attribute']['distance'] = str(self.get_distance(depth_img, landmark, unit='mm'))
                
                # face quality
                table['attribute']['over_exposure'] = str(self.fq_assessor.isOverExposurePass(ir_normalized_face))
                table['attribute']['headpose'] = [np.round(d, 2) for d in self.fq_assessor.getHeadPoseDegree(ir_normalized_face)]
                table['attribute']['blurriness'] = str(self.fq_assessor.isBlurrinessPass(ir_normalized_face))
                table['attribute']['occlusion'] = str(self.fq_assessor.isOcclusionPass(ir_normalized_face))
                table['attribute']['eye_glasses'] = ""

                # timestamp
                table['attribute']['timestamp'] = self.timestamp
                attributes.append(table)
                
        return attributes


    def get_irz_img(self, ir_img, depth_img):
        CONSTANT_DISTANCE = 800 # mm 
        # (distance / 10) // 1
        if ir_img.shape[:-1] == depth_img.shape:
            irz = np.sqrt(ir_img[:, :, 0]*1.3/255)*((depth_img/1000)+0.5)*255
            irz = np.repeat(np.expand_dims(irz,axis=2), 3, axis=2)
        else:
            irz = np.sqrt(ir_img*1.3/255)*((CONSTANT_DISTANCE/1000)+0.5)*255
        return irz

    
    def normailze_ir(self, ir_img, target_mean=128):
        """
        ir_img must be alined image
        """
        ir_img = ir_img.astype(np.float32)
        ir_img = ir_img * 255.0 / np.max(ir_img[32:104, 20:92])
        ir_face_area = ir_img[32:104, 20:92]
        no_hole_area = ir_face_area[(ir_face_area > 0)]
        shift = float(target_mean) / np.mean(no_hole_area)
        ir_img *= shift
        ir_img = np.clip(ir_img, 0, 255)
        ir_img = ir_img.astype(np.uint8)
        return ir_img

    
    def get_distance(self, depth_img, landmark, unit='m'):
        distance = -1
        s = 2
        landmark = [[int(x) for x in sublist] for sublist in landmark]
        right_eye = depth_img[landmark[0][1]-s:landmark[0][1]+s, landmark[0][0]-s:landmark[0][0]+s]
        left_eye = depth_img[landmark[1][1]-s:landmark[1][1]+s, landmark[1][0]-s:landmark[1][0]+s]
        nose = depth_img[landmark[2][1]-s:landmark[2][1]+s, landmark[2][0]-s:landmark[2][0]+s]
        right_lip = depth_img[landmark[3][1]-s:landmark[3][1]+s, landmark[3][0]-s:landmark[3][0]+s]
        left_lip = depth_img[landmark[4][1]-s:landmark[4][1]+s, landmark[4][0]-s:landmark[4][0]+s]
        
        nose = nose[(nose > 0)]
        if left_eye.size == 0 or right_eye.size == 0 or nose.size == 0 or left_lip.size == 0 or right_lip.size == 0:
            return distance
        
        if unit == 'm':
            distance = np.round(np.average(nose)*100, 2) # cm
        elif unit == 'cm':
            distance = np.round(np.average(nose), 2) # cm
        elif unit == 'mm':
            distance = np.round(np.average(nose)/10, 2) # cm
        else:
            raise ValueError()
        return distance

    
    def dump_attributes(self, spec, attributes):
        spec['dataset'] = self.config['dataset']
        spec['attribute'] = attributes
        with open(self.config['output'], 'w') as outfile:
            json.dump(spec, outfile, indent=2)
