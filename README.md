# Introduction

This repository can parse IR data with multiple attributes and create report finally.

| Support input      |
| :-----             |
| `raw`              |
| `event`            |

# Prerequisite
### KukeFace
Install from https://gitlab.com/AIImage/VisionAlgorithm/kukeface

# Usage
### 1. Modofy [config.yaml](https://gitlab.com/samtsai/data_pipeline/-/blob/main/config.yaml)

| Variable                         | Value                                                           |
| :-----                           | :----                                                           |
| `data_type`                      | `'raw'` or `'event'`                                            |

| raw_config                       | Value                                                           |
| :-----                           | :----                                                           |
| `mode`                           | `''` or `'25M'` or `'100M'`                                     |
| `root`                           | Path of `'Raw2Depth_forLinux'`                                  |
| `raw_dir`                        | Absolute path of raw image directory                            |
| `out_dir`                        | Output path of `'Raw2Depth_forLinux'`                           |
| `out_normalized_dir`             | Output path of normalized ir image, if not to save, set `''`    |

| event_config                     | Value                                                           |
| :-----                           | :----                                                           |
| `root`                           | Path of raw image directory                                     |
| `out_normalized_dir`             | Output path of normalized ir image, if not to save, set `''`    |

### 2. Run [data_pipeline.py](https://gitlab.com/samtsai/data_pipeline/-/blob/main/data_pipeline.py)
```shell
python data_pipeline.py
```