說明:

1. 指令
./RawToDepth ./20210804_dual_freq_25M_100M_test_Config.conf  ./20210804_dual_freq_25M_100M_test_Cali.conf ./imgNames.txt

2. bIRFilter
	0 : org
	1 : wiener 3
	2 : median 3
	
3. bgetmodulationFreq
	0 : Freq 1 ModulationFreq1(100M)
	1 : Freq 2 ModulationFreq1(25M)
	
4. BDualMode
	0 : single mode
	1 : dual mode