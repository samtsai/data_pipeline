def get_specification_table():
    specification_table = {
        "model": {
            "FQ": {
                "headpose": "",
                "blurriness": "",
                "occlusion": ""
            },
            "FD": {
                "stage1": "",
                "stage2": ""
            },
            "FR": ""
        },
        "attribute": []
    }
    return specification_table

def get_attribute_table():
    attribute_table = {
        "filename": "",
        "attribute": {
            "id": "",
            "distance": "",
            "over_exposure": "",
            "headpose": [],
            "blurriness": "",
            "occlusion": "",
            "eye_glasses": ""
        },
        "timestamp": ""
    }
    return attribute_table