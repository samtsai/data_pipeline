import os
import os.path as osp
import shutil
import pathlib

mode = '100M' # ['', '25M', '100M']
raw_dir = '/data_2t/sam/workspace/raw2depth/Raw2Depth_forLinux/images' # raw_dir must be absolute path
out_dir = 'out_' + mode

CONFIG_FILE = '20210804_dual_freq_25M_100M_test_Config.conf'

class RawParser(object):
    def __init__(self, root, mode, raw_dir, out_dir):
        super().__init__()
        assert mode in ['', '25M', '100M']
        self.root = root
        self.list_path = osp.join(root, 'imgNames.txt')
        self.setup_config(root, mode)
        self.setup_imgNames(raw_dir)

        # setup folder
        shutil.rmtree(out_dir, ignore_errors=True)
        os.makedirs(out_dir, exist_ok=True)

    def setup_config(self, root, mode):
        config_path = osp.join(root, CONFIG_FILE)
        with open(config_path, 'r') as f1:
            config = dict([line.split() for line in f1])
        
        config['BDualMode'] = '0' if mode == '' else '1'
        config['bgetmodulationFreq'] = '1' if mode == '25M' else '0'

        with open(config_path, 'w') as f2:
            for k, v in config.items():
                f2.write(k + ' ' + v + '\n')

    def setup_imgNames(self, raw_dir):
        assert os.path.exists(raw_dir)
        with open(self.list_path, 'w') as f:
            for raw_path in pathlib.Path(raw_dir).rglob('*.[rawpng]*'):
                f.write(str(raw_path)+'\n')
    
    # --------------------------------------------------------------------------------------
    def collect_rgb_images(self, image_list_path, raw_dir, out_dir):
        # collect all rgb images to out_dir
        missing_path = []
        rgb_dir = osp.join(out_dir, 'rgb')
        os.makedirs(rgb_dir, exist_ok=True)
        with open(image_list_path, 'r') as f1:
            raw_path = f1.readline().strip()
            while raw_path:
                rgb_path = raw_path.replace('.raw','.png').replace('raw-','rgb-')
                if not osp.exists(rgb_path):
                    missing_path.append(rgb_path)
                else:
                    dst_path = rgb_path.replace(raw_dir, rgb_dir)
                    os.rename(rgb_path, dst_path)
                raw_path = f1.readline().strip()

        # missing rgb images
        if missing_path:
            missing_rgb_path = osp.join(out_dir, 'missing_rgb.txt')
            with open(missing_rgb_path, 'w') as f2:
                for path in missing_path:
                    f2.write(path + '\n')
    
    def classify_images(self, raw_dir, out_dir):
        ext_map = {
            'ir': '.raw_confidence.bmp',
            'irz': '.raw_confidenceZmap.bmp',
            'depth': '.raw_depth.csv'
        }
        for mode, ext in ext_map.items():
            sub_dir = osp.join(out_dir, mode)
            os.makedirs(sub_dir, exist_ok=True)
            for path in pathlib.Path(raw_dir).rglob(f'*{ext}'):
                dst = str(path).replace(raw_dir, sub_dir)
                os.rename(path, dst)

    def parsing_raw(self):
        cwd = os.getcwd()
        os.chdir('./Raw2Depth_forLinux')
        opencv_lib_path = osp.join(cwd, 'Raw2Depth_forLinux', 'lib')
        os.environ['LD_LIBRARY_PATH'] += f':{opencv_lib_path}'
        print(os.environ['LD_LIBRARY_PATH'])
        cmd = './RawToDepth ./20210804_dual_freq_25M_100M_test_Config.conf ./20210804_dual_freq_25M_100M_test_Cali.conf ./imgNames.txt'
        os.system(cmd)
        os.chdir(cwd)
    

if __name__ == '__main__':
    parser = RawParser(
        root='Raw2Depth_forLinux',
        mode=mode, 
        raw_dir=raw_dir,
        out_dir=out_dir)

    parser.collect_rgb_images('Raw2Depth_forLinux/imgNames.txt', raw_dir, out_dir)
    parser.parsing_raw()
    parser.classify_images(raw_dir, out_dir)