import yaml

from raw_pipe import raw_pipe
from event_pipe import event_pipe

PIPE = {
    'raw': raw_pipe, 
    'event': event_pipe
}

class data_pipeline(object):
    def __init__(self, config_path):
        super().__init__()
        config = yaml.safe_load(open(config_path, "r"))
        data_type = config['data_type']
        assert data_type in PIPE.keys()
        
        self.pipe = PIPE[data_type](config)
    
    def run(self):
        self.pipe.run()
            
        
if __name__ == "__main__":
    pipeline = data_pipeline('config.yaml')
    pipeline.run()